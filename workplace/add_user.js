let request = require('request');
let header = require('./header_request');
let Log = require('./../log');

function add_user_to_workplace(email, name) {
    let body = {
        schemas: [
            "urn:scim:schemas:core:1.0",
            "urn:scim:schemas:extension:facebook:auth_method:1.0"
        ],
        userName: email,
        "name": {
            "formatted": name
        },
        "emails" : [{
            "primary" : true,
            "type" : "work",
            "value" : email
        }],
        "active": true,
        "urn:scim:schemas:extension:facebook:auth_method:1.0": {
            "auth_method": "sso"
        }
    };
    let options = {
        uri: 'https://www.facebook.com/scim/v1/Users',
        method: 'POST',
        headers: header.content,
        json: true,
        body: body
    };
    request(options,(error, res, body) => {
        if (!error && res.statusCode === 200) {
            Log.message(`[✅][Workplace] User: ${email} Name: '${name}' Added`);
        } else {
            Log.message(`[🔥][Workplace] Error add_user_to_workplace - User: ${email} - StatusCode: ${res.statusCode} - Error: ${error}`);
        }
    });
}

function manage_gold_subscription(method, email) { // method POST o DELETE
    let gold_group = '254053725438764';
    let email_url_encoded = encodeURI(email);
    let options = {
        uri: `https://graph.facebook.com/${gold_group}/members?email=${email_url_encoded}`,
        method: method,
        headers: header.content
    };

    request(options,(error, res, body) => {
        if (!error && res.statusCode === 200) {
            Log.message(`[✅][Workplace] User: ${email} ${method} group Gold`);
        } else {
            Log.message(`[🔥][Workplace] Error add_to_gold_group - User: ${email} - StatusCode: ${res.statusCode} - Error: ${error}`);
        }
    });

}

exports.sign_user = add_user_to_workplace;
exports.manage_gold_subscription = manage_gold_subscription;