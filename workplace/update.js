let request = require('request');
let header = require('./header_request');
let Log = require('./../log');

function update_user_photoURL(req, res) {
    let email = req.query.email;
    let photoURL = req.query.photoURL;

    get_user_id(email, (id, active) => {
        let body = {
            "schemas": [
                "urn:scim:schemas:core:1.0"
            ],
            "active": active,
            "photos": [{
                "value": photoURL,
                "type": "profile",
                "primary": true
            }]
        };

        let options = {
           uri: `https://www.facebook.com/scim/v1/Users/${id}`,
           method: 'PUT',
           headers: header.content,
           json: true,
           body: body
        };

        request(options,(error, response, body) => {
            if (!error && response.statusCode === 200) {
                Log.message(`[✅][Workplace] Photo Updated, user: '${email}' with image: ${photoURL}`);
            } else {
                Log.message(`[🔥][House] Error Workplace update_user_photoURL - User: ${email} - StatusCode: ${response.statusCode} - Error: ${error}`);
            }
        });
    });
}

function get_user_id(email, completionHandler) {
    let options = {
        uri: `https://www.facebook.com/scim/v1/Users?filter=userName%20eq%20%22${email}%22`,
        method: 'GET',
        headers: header.content
    };

    request(options,(error, res, body) => {
        if (!error && res.statusCode === 200) {
            let json = JSON.parse(body);
            let id = json['Resources'][0]['id'];
            let active = json['Resources'][0]['active'];
            Log.message(`[✅][Workplace] Get User ID: ${id} from ${email}`);
            completionHandler(id, active);
        } else {
            Log.message(`[🔥][House] Error Workplace get_user_id - User: ${email} - StatusCode: ${res.statusCode} - Error: ${error}`);
        }
    });
}


exports.update_user_photoURL = update_user_photoURL;
exports.get_user_id = get_user_id;