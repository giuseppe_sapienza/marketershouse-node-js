let request = require('request');
let header = require('./header_request');

async function get_group_feed(server) {

    let gold_feed_option = {
        uri: 'https://graph.facebook.com/312520369482402/feed',
        method: 'GET',
        headers: header.content
    };

    request(gold_feed_option, (error, res, body) => {
        if (!error && res.statusCode === 200) {
            let json = JSON.parse(body);
            let posts = json['data'].slice(0, 3);

            var array = [];
            for (var i = 0, len = posts.length; i < len; i++) {
                let post = posts[i];

                get_post(post['id'], (data) => {
                    array.push(data);

                    if (array.length === posts.length) {
                        array.sort((a, b) => {
                            return new Date(b.date) - new Date(a.date);
                        });
                        server.send(array);
                    }
                }, error => {
                    server.send({error: 'error'});
                });
            }

        } else {
            console.log(`[🔥][Workplace] Error - StatusCode: ${res.statusCode} - Error: ${error}`);
        }
    });
}

function get_post(post_id, completion, errorHandler) {
    let post_option = {
        uri: `https://graph.facebook.com/${post_id}?fields=from,created_time,icon,permalink_url,message`,
        method: 'GET',
        headers: header.content
    };

    request(post_option, (error1, res1, body1) => {
        if (!error1 && res1.statusCode === 200) {
            let body = JSON.parse(body1);
            let user = body['from'];
            let user_id = user['id'];
            let user_name = user['name'];
            let message = body['message'];
            let date = body['created_time'];
            let link = body['permalink_url'];


            let user_option = {
                uri: `https://graph.facebook.com/${user_id}?fields=picture`,
                method: 'GET',
                headers: header.content
            };

            request(user_option, (error2, res2, body2) => {
                if (!error2 && res2.statusCode === 200) {
                    let user_body = JSON.parse(body2);
                    let picture_url = user_body['picture']['data']['url'];

                    let final = {
                        user_id: user_id,
                        user_name: user_name,
                        user_url: picture_url,
                        message: message,
                        link: link,
                        date: date
                    };

                    completion(final);
                } else {
                    errorHandler();
                }
            });

        } else {
            errorHandler();
        }
    });
}

exports.get_group_feed = get_group_feed;