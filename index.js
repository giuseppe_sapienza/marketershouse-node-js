const express = require('express');
const app = express();

const bodyParser = require('body-parser');

let user_tag_webhook = require('./activecampaign/tag');
let user_webhook = require('./activecampaign/contact');

let workplace = require('./workplace/update');

let workplace_feed = require('./workplace/group_feed');
let Log = require('./log');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.listen(3000, () => {
    console.log('Server running!');
});

app.get('/feed', (req, res) => {
    workplace_feed.get_group_feed(res);
});

app.post('/webhook/ac/tag_added', (req, res) => {
    user_tag_webhook.added(req, res);
});

app.post('/webhook/ac/tag_removed', (req, res) => {
    user_tag_webhook.removed(req, res);
});

app.post('/webhook/ac/contact_updated', (req, res) => {
    user_webhook.updated(req, res);
});

app.post('/webhook/wp/user_session', (req, res) => {
    let user = req.body['entry'][0]['changes'][0]['value'];
    Log.message(`[✅][Workplace] ${user['actor_email']} logged in`);
    res.send('OK');
});

app.post('/webhook/wp/posts', (req, res) => {

    var i;
    let entry_array = req.body['entry'];

    //console.log(entry_array);

    for (i = 0; i < entry_array.length; i++) {

        let entry = entry_array[i];
        let change_array = entry['changes'];
        //console.log(change_array);

        var j;
        for(j = 0; j < change_array.length; j++) {
            let change = change_array[j];
            console.log(change);

            let field = change['field']; // comments - posts

            let post = change['value'];
            let verb = post['verb']; // add - edit - delete
            let message = post['message'];

            console.log(entry);

            if (verb === 'delete') {
                if (field === 'comments') {
                    // commento cancellato
                }

                if (field === 'posts') {
                    //
                }
            }

            if (verb === 'add') {
                let user = post['from'];
                let name = user['name'];

                if (field === 'comments') {
                    // commento aggiunto
                    Log.message(`[✅][Workplace] user ${name} added a comment`)
                }

                if (field === 'posts') {
                    Log.message(`[✅][Workplace] user ${name} added a post`)
                }

                //Log.message();
            }
        }
    }

    res.send('OK');
});

app.get('/workplace/user/update/photo', (req, res) => {
    workplace.update_user_photoURL(req, res);
    res.send({status: 'OK!'});
});

app.get('/test', (req, res) => {
    console.log('[TEST] GET OK');
    res.send('ok');
});
