let request = require('request');
let Tag = require('./enum_tag');
let Log = require('./../log');


function contact_updated(req, res) {
    let tags = req.body['contact[tags]'];

    if (tags === undefined || tags === null || tags === '') {
        res.send({status: 'OK'});
        return;
    }

    if (tags.includes(Tag.value.account)) {
        let email = req.body['contact[email]'];

        console.log(`[✅][WebHook] Received User Update: ${email}`);

        send_body_request_to_house(req.body);

        if (tags.includes(Tag.value.firstPsw) || tags.includes(Tag.value.newPsw)) {
            try_to_update_user_password(req.body);
        }
    }

    res.send({status: 'OK'});
}


function send_body_request_to_house(body) {
    let options1 = {
        uri: 'https://door.marketershouse.net/?type=contact&key=mkts19xc44',
        method: 'POST',
        form: body
    };

    let email = body['contact[email]'];

    request(options1,(error, res, body) => {
        if (!error && res.statusCode === 200) {
            console.log(`[✅][House] User: ${email} info sent`);
        } else {
            Log.message(`[🔥][House] Error send_body_request_to_house - User: ${email} - StatusCode: ${res.statusCode} - Error: ${error}`)
        }
    });
}


function try_to_update_user_password(body) {
    if (body['contact[fields][15]'] !== '') {
        let email = body['contact[email]'];
        let psw = body['contact[fields][15]'];

        let options2 = {
            uri: 'https://door.marketershouse.net/wp-json/mkts/v1/ac/update?api_key=Mxcoding19',
            method: 'POST',
            json: true,
            body: {
                email: email,
                psw: psw
            }
        };

        request(options2,(error, res, body) => {
            if (!error && res.statusCode === 200) {
                Log.message(`[✅][House] User: ${email} Password: '${psw}' Updated`);
            } else {
                Log.message(`[🔥][House] Error try_to_update_user_password - User: ${email} Password: '${psw}' - StatusCode: ${res.statusCode} - Error: ${error}`);
            }
        });
    }
}


exports.updated = contact_updated;