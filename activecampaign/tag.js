let workplace = require('./../workplace/add_user');
let Tag = require('./enum_tag');
let Log = require('./../log');

function tag_added(req, res) {
    let tag = req.body['tag'];

    if (!tag.includes('MH:')) {
        res.send( {status: ':( Contact discarded' });
        return;
    }

    let email = req.body['contact[email]'];
    Log.message(`[✅][WebHook] Tag: '${tag}' added to: ${email}`);

    if (tag === Tag.value.account) {
        let name = req.body['contact[first_name]'];
        let surname = req.body['contact[last_name]'];
        let complete_name = `${name} ${surname}`;
        workplace.sign_user(email, complete_name);
    }

    if (tag === Tag.value.gold) {
        workplace.manage_gold_subscription('POST', email);
    }

    res.send({status: 'OK'});
}

function tag_removed(req, res) {
    let tag = req.body['tag'];

    if (!tag.includes('MH:')) {
        res.send( {status: ':( Contact discarded' });
        return;
    }

    let email = req.body['contact[email]'];
    Log.message(`[✅][WebHook] Tag: '${tag}' removed from: ${email}`);

    if (tag === Tag.value.gold) {
        workplace.manage_gold_subscription('DELETE', email);
    }

    res.send({status: 'OK'});
}

exports.added = tag_added;
exports.removed = tag_removed;