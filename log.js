let request = require('request');

function log(message) {
    console.log(message);
    let option = {
        uri: 'https://hooks.slack.com/services/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',
        method: 'POST',
        json: true,
        body: {
            text: message
        }
    };

    request(option,(error, res, body) => {
    });
}

exports.message = log;